package org.microservice.controller;

import lombok.RequiredArgsConstructor;
import org.microservice.dto.Student;
import org.microservice.dto.StudentRequest;
import org.microservice.dto.StudentResponse;
import org.microservice.service.StudentService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService service;

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable Long id) {
        return service.getStudentById(id);
    }

    @GetMapping("/list")
    public List<Student> getAllStudents() {
        return service.getAllStudents();
    }

    @DeleteMapping("/delete/{id}")
    public void deleteStudentById(@PathVariable Long id) {
        service.deleteStudentById(id);
    }

    @PostMapping("/insert")
    public StudentResponse insertStudent(@RequestBody StudentRequest request) {
        return new StudentResponse(service.addStudent(request));
    }

    @PutMapping("/update/{id}")
    public Student updateStudent(@PathVariable Long id, @RequestBody StudentRequest request) {
        return service.updateStudent(id, request);
    }

}
