package org.microservice.service;

import lombok.RequiredArgsConstructor;
import org.microservice.dto.Student;
import org.microservice.dto.StudentRequest;
import org.microservice.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;
    private final ModelMapper modelMapper;


    @Override
    public Student getStudentById(Long id) {
        org.microservice.entity.Student student = repository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Student id {" + id + "} not found "));
        return modelMapper.map(student, Student.class);
    }

    @Override
    public List<Student> getAllStudents() {
        List<org.microservice.entity.Student> students = repository.findAll();
        return students.stream()
                .map(student -> modelMapper.map(student, Student.class))
                .collect(Collectors.toList());
    }

    @Override
    public Student addStudent(StudentRequest request) {
        org.microservice.entity.Student student = modelMapper.map(request, org.microservice.entity.Student.class);
        return modelMapper.map(repository.save(student), Student.class);
    }

    @Override
    public Student updateStudent(Long id, StudentRequest request) {
        org.microservice.entity.Student student = modelMapper.map(request, org.microservice.entity.Student.class);
        student.setId(id);
        repository.save(student);

        return modelMapper.map(student, Student.class);

    }


    @Override
    public void deleteStudentById(Long id) {
        getStudentById(id);
        repository.deleteById(id);
    }

}
