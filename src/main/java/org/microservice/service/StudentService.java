package org.microservice.service;


import org.microservice.dto.Student;
import org.microservice.dto.StudentRequest;

import java.util.List;

public interface StudentService {

    Student getStudentById(Long id);

    List<Student> getAllStudents();

    void deleteStudentById(Long id);

    Student addStudent(StudentRequest request);

    Student updateStudent(Long id ,StudentRequest request);

}
