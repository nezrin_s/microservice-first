package org.microservice.dto;

import lombok.Data;

@Data
public class Student {

    private Long id;
    private String name;
    private String surname;
    int age;

}
