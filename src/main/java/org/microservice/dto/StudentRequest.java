package org.microservice.dto;

import lombok.Data;

@Data
public class StudentRequest {

    String name;
    String surname;
    int age;

}


